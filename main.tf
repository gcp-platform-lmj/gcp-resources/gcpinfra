terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }

  #  backend "gcs" {
  #    bucket  = "your-bucket-name"
  #    prefix  = "terraform/state"
  #    credentials = "path/to/credentials.json"
  #  }
}

provider "google" {
  project = "project-gke-2023"
  region  = "asia-east1"
  zone    = "asia-east1-a"
}


resource "google_compute_instance" "vm_instance" {
  name         = "my-vm-instance"
  machine_type = "n1-standard-1"
  zone         = "asia-east1-a"
  tags         = ["dev", "webserver"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  network_interface {
    network = "vpc-demo"
    subnetwork = "subnet-k8s"
    access_config {
    }
  }
}
